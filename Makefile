DFD_SOURCES=$(shell find img/*.dfd)
DFD_TARGETS=$(DFD_SOURCES:img/%.dfd=img/%.dfd.svg)
SEQ_SOURCES=$(shell find img/*.seq)
SEQ_TARGETS=$(SEQ_SOURCES:img/%.seq=img/%.seq.svg)
UML_SOURCES=$(shell find img/*.uml)
UML_TARGETS=$(DFD_SOURCES:img/%.uml=img/%.uml.svg)

%.dfd.svg: %.dfd
	@dataflow dfd $< | dot -Tsvg > $@

%.seq.svg: %.seq
	@dataflow seq $< | java -Djava.awt.headless=true -jar /usr/lib/plantuml.jar -tsvg -pipe > $@

dfd: $(DFD_TARGETS)

seq: $(SEQ_TARGETS)

doc:
	@gitbook build . public

clean:
	rm -f $(DFD_TARGETS)
	rm -f $(SEQ_TARGETS)
	rm -f public

all: doc
