# DFD Example

The following SVG images are generated automatically by transforming
[this file](https://framagit.org/promethe42/dataflow-gitlab-ci/blob/master/img/webapp.flow)
using [Dataflow](https://github.com/sonyxperiadev/dataflow) and [Graphviz](http://graphviz.org/) running in GitLab CI:

![Web App](img/webapp.dfd.svg)

Note: for this to work you must **not** view this content in the GitLab repository, but in the [corresponding GitLab
Pages](http://promethe42.frama.io/dataflow-gitlab-ci/DFD_Example.html).
