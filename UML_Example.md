# UML Example

The following SVG images are generated automatically by transforming
[this file](https://framagit.org/promethe42/dataflow-gitlab-ci/blob/master/img/webapp.flow)
using [Dataflow](https://github.com/sonyxperiadev/dataflow) and [PlantUML](http://plantuml.com/) running in GitLab CI:

![Web App](img/webapp.seq.svg)

Note: for this to work you must **not** view this content in the GitLab repository, but in the [corresponding GitLab
Pages](http://promethe42.frama.io/dataflow-gitlab-ci/UML_Example.html).
