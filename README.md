# DFD/UML + GitLab CI

A sample repository to show the combined use of:

* [Dataflow](https://github.com/sonyxperiadev/dataflow)
* [Graphviz](http://graphviz.org/)
* [PlantUML](http://plantuml.com/)
* [GitBook](https://gitbook.io)

to generate HTML static sites including SVG graphs generated from [DFD](http://en.wikipedia.org/wiki/Data_flow_diagram),
[sequence diagrams](http://plantuml.sourceforge.net/sequence.html) or UML.

This repository features a GitLab CI configuration file that automates the process of converting the graph files
into SVG images embedded in the generated GitLab Pages website.
